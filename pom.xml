<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>

	<groupId>co.brilliantfire.cloud</groupId>
	<artifactId>config-server</artifactId>
	<version>0.1.1-SNAPSHOT</version>
	<packaging>jar</packaging>

	<name>cloud-config-server</name>
	<description>Service for providing central configuration to a distributed environment</description>
	
	<properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<java.version>1.8</java.version>
		<spring-cloud.version>Dalston.SR1</spring-cloud.version>
		<docker.image.prefix>springcloud</docker.image.prefix>
	</properties>
	<parent>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-parent</artifactId>
		<version>1.5.4.RELEASE</version>
		<relativePath /> <!-- lookup parent from repository -->
	</parent>
	<dependencies>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-config</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-eureka</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-config-server</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-config-monitor</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.cloud</groupId>
			<artifactId>spring-cloud-starter-stream-rabbit</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-devtools</artifactId>
		</dependency>
		<dependency>
			<groupId>org.projectlombok</groupId>
			<artifactId>lombok</artifactId>
		</dependency>
		<dependency>
			<groupId>org.eclipse.jgit</groupId>
			<artifactId>org.eclipse.jgit</artifactId>
			<version>4.8.0.201706111038-r</version>
		</dependency>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
		
	</dependencies>

	<dependencyManagement>
		<dependencies>
			<dependency>
				<groupId>org.springframework.cloud</groupId>
				<artifactId>spring-cloud-dependencies</artifactId>
				<version>${spring-cloud.version}</version>
				<type>pom</type>
				<scope>import</scope>
			</dependency>
		</dependencies>
	</dependencyManagement>

	<build>
		<plugins>
			<plugin>
				<groupId>org.projectlombok</groupId>
				<artifactId>lombok-maven-plugin</artifactId>
				<version>1.16.18.0</version>
				<executions>
					<execution>
						<phase>generate-sources</phase>
						<goals>
							<goal>delombok</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>com.spotify</groupId>
				<artifactId>docker-maven-plugin</artifactId>
				<version>0.2.3</version>
				<configuration>
					<imageName>${docker.image.prefix}/${project.artifactId}</imageName>
					<dockerDirectory>src/main/docker</dockerDirectory>
					<resources>
						<resource>
							<targetPath>/</targetPath>
							<directory>${project.build.directory}</directory>
							<include>${project.build.finalName}.jar</include>
						</resource>
					</resources>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.springframework.boot</groupId>
				<artifactId>spring-boot-maven-plugin</artifactId>
				<configuration>
					<classifier>exec</classifier>
				</configuration>
			</plugin>
			<plugin>
				<groupId>pl.project13.maven</groupId>
				<artifactId>git-commit-id-plugin</artifactId>
				<configuration>
					<failOnNoGitDirectory>false</failOnNoGitDirectory>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-javadoc-plugin</artifactId>
				<!-- <version>2.10.3</version> -->
				<executions>
					<execution>
						<id>attach-javadocs</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-source-plugin</artifactId>
				<version>3.0.1</version>
				<executions>
					<execution>
						<id>attach-sources</id>
						<goals>
							<goal>jar</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>1.8</source>
					<target>1.8</target>
				</configuration>
			</plugin>
			<!-- <plugin> <artifactId>maven-deploy-plugin</artifactId> <configuration> 
				<detail>true</detail> <skip>true</skip> </configuration> </plugin> -->
			<plugin>
				<groupId>org.jfrog.buildinfo</groupId>
				<artifactId>artifactory-maven-plugin</artifactId>
				<version>2.6.1</version>
				<inherited>false</inherited>
				<configuration>
					<publisher>
						<maven>true</maven>
						<contextUrl>${server.repo.contextUrl}</contextUrl>
						<username>${server.repo.username}</username>
						<password>${server.repo.password}</password>
						<repoKey>libs-release-local</repoKey>
						<snapshotRepoKey>libs-snapshot-local</snapshotRepoKey>
						<excludePatterns>*-tests.jar</excludePatterns>
						<publishArtifacts>true</publishArtifacts>
						<publishBuildInfo>true</publishBuildInfo>
						<recordAllDependencies>true</recordAllDependencies>
						<m2Compatible>true</m2Compatible>
					</publisher>
					<artifactory>
						<envVarsExcludePatterns>server.repo.password,server.repo.username</envVarsExcludePatterns>
						<includeEnvVars>true</includeEnvVars>
						<activateRecorder>true</activateRecorder>
					</artifactory>
					<deployProperties>
						<groupId>${project.groupId}</groupId>
						<artifactId>${project.artifactId}</artifactId>
						<version>${project.version}</version>
						<packageType>maven</packageType>
						<!-- <fileItegRev>${git.branch}-${git.commit.id.describe-short}</fileItegRev> -->
					</deployProperties>
				</configuration>
				<executions>
					<execution>
						<id>build-info</id>
						<phase>package</phase>
						<goals>
							<goal>publish</goal>
						</goals>
						<inherited>false</inherited>
					</execution>
				</executions>
			</plugin>
		</plugins>

	</build>

	<distributionManagement>
		<repository>
			<id>central</id>
			<name>Brilliantfire Design Artifact Repository-releases</name>
			<url>http://192.168.1.174:8081/artifactory/libs-release-local</url>
			<uniqueVersion>true</uniqueVersion>
			<layout>default</layout>
		</repository>
		<snapshotRepository>
			<id>snapshots</id>
			<name>Brilliantfire Design Artifact Repository-snapshots</name>
			<url>http://192.168.1.174:8081/artifactory/libs-snapshot-local</url>
			<uniqueVersion>false</uniqueVersion>

		</snapshotRepository>
	</distributionManagement>
	<repositories>
		<repository>
			<id>central</id>
			<name>Brilliantfire Design Artifact Repository-releases</name>
			<url>http://192.168.1.174:8081/artifactory/libs-release</url>
			<snapshots>
				<enabled>false</enabled>
			</snapshots>
			<layout>default</layout>
			<releases>
				<enabled>true</enabled>
				<checksumPolicy>fail</checksumPolicy>
			</releases>
		</repository>
		<repository>
			<id>snapshots</id>
			<name>Brilliantfire Design Artifact Repository-snapshots</name>
			<url>http://192.168.1.174:8081/artifactory/libs-snapshot</url>
			<snapshots>
				<enabled>true</enabled>
				<updatePolicy>always</updatePolicy>
				<checksumPolicy>fail</checksumPolicy>
			</snapshots>
			<layout>default</layout>
			<releases>
				<enabled>false</enabled>
			</releases>
		</repository>
		<!-- <repository> <id>spring-snapshots</id> <name>Spring Snapshots</name> 
			<url>https://repo.spring.io/libs-snapshot-local</url> <snapshots> <enabled>true</enabled> 
			</snapshots> </repository> <repository> <id>spring-milestones</id> <name>Spring 
			Milestones</name> <url>https://repo.spring.io/libs-milestone-local</url> 
			<snapshots> <enabled>false</enabled> </snapshots> </repository> <repository> 
			<id>spring-releases</id> <name>Spring Releases</name> <url>https://repo.spring.io/release</url> 
			<snapshots> <enabled>false</enabled> </snapshots> </repository> -->
	</repositories>
</project>
