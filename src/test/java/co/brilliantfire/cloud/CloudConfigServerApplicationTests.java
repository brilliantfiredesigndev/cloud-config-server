package co.brilliantfire.cloud;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@TestPropertySource(locations = {"classpath:test.properties", "classpath:test.yml"})
@ActiveProfiles(profiles = {"test"}) 
@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest//(classes = {CloudConfigServerApplication.class}, properties = {"server.port=0"})
public class CloudConfigServerApplicationTests {

	
	
	@Value("${local.server.port}")
	private int port = 0;

	@Test
	public void successfulTest() {
		assertEquals(HttpStatus.OK, HttpStatus.OK);
	}
	
//	@Test
//	public void contextLoads() {
//	}
	
//	@Test
//	public void configurationAvailable() {
//		@SuppressWarnings("rawtypes")
//		ResponseEntity<Map> entity = new TestRestTemplate().getForEntity(
//				"http://localhost:" + port + "/app/cloud", Map.class);
//		assertEquals(HttpStatus.OK, entity.getStatusCode());
//	}
//
//	@Test
//	public void envPostAvailable() {
//		MultiValueMap<String, String> form = new LinkedMultiValueMap<String, String>();
//		@SuppressWarnings("rawtypes")
//		ResponseEntity<Map> entity = new TestRestTemplate().postForEntity(
//				"http://localhost:" + port + "/admin/env", form, Map.class);
//		assertEquals(HttpStatus.OK, entity.getStatusCode());
//	}

}
