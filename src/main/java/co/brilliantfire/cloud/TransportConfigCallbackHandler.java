package co.brilliantfire.cloud;

import org.eclipse.jgit.api.TransportConfigCallback;
import org.eclipse.jgit.transport.Transport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

//@RefreshScope(proxyMode = ScopedProxyMode.TARGET_CLASS)
@Slf4j
@Configuration
public class TransportConfigCallbackHandler implements TransportConfigCallback {

	public TransportConfigCallbackHandler() {
		log.info("TransportCOnfigCallbackHandler constructed!");
	}
	@Autowired(required = false)
	private TransportConfigCallback transportConfigCallback;
	
	public TransportConfigCallback getTransportConfigCallback() {
		log.info("Returning private var transportConfigCallback via getter request: {}", this.transportConfigCallback);
		return transportConfigCallback;
	}

	public void setTransportConfigCallback(TransportConfigCallback transportConfigCallback) {
		log.info("Setting transportConfigCallback by setter request: {}", transportConfigCallback);
		this.transportConfigCallback = transportConfigCallback;
	}
//	public TransportConfigCallbackHandler() {
//	}
	
//	public static TransportConfigCallback getTransportConfigCallback() {
//		
//		return new TransportConfigCallback() {
//		
//		
//			 
//            public void configure(Transport transport) { 
//            	log.info("Transport Configuration callback called");
//        		log.info("Trans conf CredentailsProvider: {}", transport.getCredentialsProvider());
//        		log.info("Trans conf OptionReceivePack: {}", transport.getOptionReceivePack());
//        		log.info("Trans conf getOptionUploadPack: {}", transport.getOptionUploadPack());
//        		log.info("Trans conf getPackConfig: {}", transport.getPackConfig());
//        		log.info("Trans conf getPushOptions: {}", transport.getPushOptions());
//        		log.info("Trans conf getTagOpt: {}", transport.getTagOpt());
//        		log.info("Trans conf getURI: {}", transport.getURI());
//        		log.info("Trans conf getTransportProtocolsr: {}", Transport.getTransportProtocols());
//        		log.info("End of callback read");
//            } 
//        }; 
//	}

	@Override
	public void configure(Transport transport) {
		
        	log.info("Transport Configuration callback called");
    		log.info("Trans conf CredentailsProvider: {}", transport.getCredentialsProvider());
    		log.info("Trans conf OptionReceivePack: {}", transport.getOptionReceivePack());
    		log.info("Trans conf getOptionUploadPack: {}", transport.getOptionUploadPack());
    		log.info("Trans conf getPackConfig: {}", transport.getPackConfig());
    		log.info("Trans conf getPushOptions: {}", transport.getPushOptions());
    		log.info("Trans conf getTagOpt: {}", transport.getTagOpt());
    		log.info("Trans conf getURI: {}", transport.getURI());
    		log.info("Trans conf getTransportProtocolsr: {}", Transport.getTransportProtocols());
    		log.info("End of callback read");
        
		
	}
	
}
