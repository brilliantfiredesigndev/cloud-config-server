package co.brilliantfire.cloud;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.jgit.transport.JschConfigSessionFactory;
import org.eclipse.jgit.transport.OpenSshConfig.Host;
import org.eclipse.jgit.util.FS;
import org.springframework.util.StreamUtils;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "co.brilliantfire.cloud - Factory")
public class FixedSshSessionFactory extends JschConfigSessionFactory {

    protected URL[] identityKeyURLs;

    /**
     * @param url
     */
    public FixedSshSessionFactory(URL... identityKeyURLs) {
    		log.info("Beginning read of instantiation parameter [URL] identityKeyUrls");
    		for ( URL url : identityKeyURLs ) {
    			log.info("Identity Key URL: {}", url);
    		}
        this.identityKeyURLs = identityKeyURLs;
    }

    /* (non-Javadoc)
     * @see org.eclipse.jgit.transport.JschConfigSessionFactory#configure(org.eclipse.jgit.transport.OpenSshConfig.Host, com.jcraft.jsch.Session)
     */
    @Override
    protected void configure(Host hc, Session session) {
        // nothing special needed here.
    		log.info("configure hc: {}\t\t session: {}", hc, session);
    }

    /* (non-Javadoc)
     * @see org.eclipse.jgit.transport.JschConfigSessionFactory#getJSch(org.eclipse.jgit.transport.OpenSshConfig.Host, org.eclipse.jgit.util.FS)
     */
    @Override
    protected JSch getJSch(Host hc, FS fs) throws JSchException {
        JSch jsch = super.getJSch(hc, fs);
        log.info("getJsch return from super: in - hc: {} \t\t fs: {}\t out - jsch: {}", hc, fs, jsch);
        // Clean out anything 'default' - any encrypted keys
        // that are loaded by default before this will break.
        jsch.removeAllIdentity();
        int count = 0;
        for (final URL identityKey : identityKeyURLs) {
        		log.info("Opening stream for identityKey: {}", identityKey);
        		log.info("Keys in jsch: {}", count);
            try (InputStream stream = identityKey.openStream()) {
            		log.info("Copying stream as byte array to jsch identitys");
                jsch.addIdentity("key" + ++count, StreamUtils.copyToByteArray(stream), null, null);
            } catch (IOException e) {
                log.error("Failed to load identity " + identityKey.getPath());
            }
        }
        return jsch;
    }


}