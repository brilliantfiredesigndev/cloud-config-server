package co.brilliantfire.cloud;

import javax.annotation.PostConstruct;

import org.eclipse.jgit.events.ConfigChangedEvent;
import org.eclipse.jgit.events.IndexChangedEvent;
import org.eclipse.jgit.events.RepositoryEvent;
import org.springframework.amqp.event.AmqpEvent;
import org.springframework.amqp.rabbit.core.RabbitAdminEvent;
import org.springframework.amqp.rabbit.listener.AsyncConsumerRestartedEvent;
import org.springframework.amqp.rabbit.listener.AsyncConsumerStartedEvent;
import org.springframework.amqp.rabbit.listener.AsyncConsumerStoppedEvent;
import org.springframework.amqp.rabbit.listener.ListenerContainerConsumerFailedEvent;
import org.springframework.amqp.rabbit.listener.ListenerContainerIdleEvent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.discovery.event.HeartbeatEvent;
import org.springframework.cloud.context.environment.EnvironmentChangeEvent;
import org.springframework.cloud.netflix.eureka.CloudEurekaClient;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Component;
import org.springframework.web.context.support.ServletRequestHandledEvent;
import org.springframework.web.servlet.DispatcherServlet;

import lombok.extern.slf4j.Slf4j;

@Slf4j(topic = "Event")
@Component
public class ConnectionListenerConfig {

	private static final String LINE = "------------------------------------------------------------";

	@Value( "${spring.application.name}")
	private String name;
	
	@Value( "${spring.profiles.active}")
	private String profile;
	
	private String label = "master";
	
	public ConnectionListenerConfig() {
		log.info("Connection Listener Config created.");
//		raw/{label}/{profile}/{profile}-{application}.*
		log.info("git path generated: row/{}/{}/{}-{}.*", label, profile, profile, name );
	
	}
	
	@PostConstruct
	private void init() {
		log.info("Connection Listener post construct called.");
		log.info("git path generated: row/{}/{}/{}-{}.*", label, profile, profile, name );
	}

	@EventListener
	public void onApplicationEvent(ApplicationEvent event) {
		if ( !event.getClass().equals(ServletRequestHandledEvent.class ) ) {
			
			if (event.getClass().equals(HeartbeatEvent.class)) {
				startEvent();
				System.out.printf("%s : %s\t%s\n%s", "ApplicationEvent", event.getTimestamp(),
						"HeartbeatEvent - CloudEurekaClient : elapsed beat time: ",
						((CloudEurekaClient) event.getSource()).getLastSuccessfulHeartbeatTimePeriod() + "\n\n\n");
				
			} else {
				printEvent("ApplicationEvent : " + event.getClass().getSimpleName(), event.getTimestamp(), event.getClass(), event.getSource());
			}
		}
		
	}

	@EventListener
	public void onServletRequestHandledEvent(ServletRequestHandledEvent event) {
		System.out.printf("\n\n%s\n", LINE);
		System.out.printf("%s - [ %s ] %s : %s\n", event.getMethod(), "ServletRequestHandledEvent", event.getRequestUrl(), event.getStatusCode());
		System.out.println("Description");
		System.out.println(event.getShortDescription());
		System.out.printf("\n%s\n", event.getDescription());
		if ( event.getSource().getClass().equals(DispatcherServlet.class) ) {
			System.out.println("Dispatcher Servlet config: " + ((DispatcherServlet) event.getSource()).getServletConfig());
		} else {
			System.out.println(event.getSource().getClass() + " is not an instance of " + DispatcherServlet.class);
		}
		System.out.println(event.getSource().getClass().getName());
		System.out.printf("\nFailure Cause: %s\n", event.getFailureCause());
		System.out.println(LINE);
	}

	@EventListener
	public void onContextRefreshedEvent(ContextRefreshedEvent event) {
		printEvent("ContextRefreshEvent : " + event.getApplicationContext().getId(), event.getTimestamp(),
				event.getClass(), event.getSource());

	}

	@EventListener
	public void onIndexChangedEvent(IndexChangedEvent event) {
		log.info("IndexChangedEvent: [] : []  |  []", event.getListenerType().getSimpleName(),
				event.getClass().getSimpleName(), event.getRepository().getListenerList());
		// IndexChangedListener
		// extends RepositoryEvent<IndexChangedListener>
	}

	@EventListener
	public void onConfigChangedEvent(ConfigChangedEvent event) {
		log.info("ConfigChangedEvent: []", event);
		// ConfigChangedListener
		// extends RepositoryEvent<ConfigChangedListener>
	}

	@EventListener
	public void onRepositoryEvent(RepositoryEvent<?> event) {
		log.info("RepositoryEvent: []", event);
		// RepositoryListener
	}

	@EventListener
	public void onEnvironmentChangeEvent(EnvironmentChangeEvent event) {
		printEvent("ApplicationEvent : " + event.getClass().getSimpleName(), event.getTimestamp(),
				event.getSource().getClass().getSimpleName(), event.getSource());
	}

	@EventListener
	public void onAmqpEvent(AmqpEvent event) {
		printEvent("AmqpEvent", event.getTimestamp(), event.getClass(), event.getSource());
	}

	@EventListener
	public void onRabbitAdminEvent(RabbitAdminEvent event) {
		printEvent("RabbitAdminEvent", event.getTimestamp(), event.getClass(), event.getSource());
	}

	@EventListener
	public void onAmqpEvent(AsyncConsumerRestartedEvent event) {
		printEvent("AsyncConsumerRestartedEvent\n" + event.getNewConsumer() + "\n" + event.getOldConsumer(),
				event.getTimestamp(), event.getClass(), event.getSource());
	}

	@EventListener
	public void onAmqpEvent(AsyncConsumerStartedEvent event) {
		printEvent("AsyncConsumerStartedEvent\n" + event.getConsumer(), event.getTimestamp(), event.getClass(),
				event.getSource());
	}

	@EventListener
	public void onAmqpEvent(AsyncConsumerStoppedEvent event) {
		printEvent("AsyncConsumerStoppedEvent\n" + event.getConsumer(), event.getTimestamp(), event.getClass(),
				event.getSource());
	}

	@EventListener
	public void onAmqpEvent(ListenerContainerConsumerFailedEvent event) {
		printEvent("ListenerContainerConsumerFailedEvent\n" + event.getReason(), event.getTimestamp(), event.getClass(),
				event.getSource());
	}

	@EventListener
	public void onAmqpEvent(ListenerContainerIdleEvent event) {
		printEvent("ListenerContainerIdleEvent\n" + event.getListenerId() + " | " + event.getIdleTime() + "\n"
				+ event.getQueueNames(), event.getTimestamp(), event.getClass(), event.getSource());
	}

	public boolean supportsEventType(ResolvableType eventType) {
		System.out.println("ResolvableType: " + eventType);
		return false;
	}

	public boolean supportsSourceType(Class<?> sourceType) {
		System.out.println("Class: " + sourceType);
		return false;
	}

	private void printEvent(String header, long timestamp, Class<? extends ApplicationEvent> clazz, Object source) {
		printEvent(header, timestamp, clazz.getSimpleName(), source);
	}

	private void printEvent(String header, long timestamp, String clazz, Object source) {
		startEvent();
		StringBuffer sb = new StringBuffer();
		sb.append(header).append("\n");
		sb.append(timestamp).append("\n");
		sb.append(clazz).append("\n");
		sb.append(source).append("\n\n");
		System.out.println(sb.toString());
	}

	private void startEvent() {
		System.out.printf("\n\n++++++++++\n%s\n", LINE);
	}
}
